package com.example.provaanagrama;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class FragmentAnagram extends Fragment {
    EditText edPalavra, edNumero;

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_anagram_inputs, container, false);

        edPalavra = (EditText) v.findViewById(R.id.edPalavra);
        edNumero = (EditText) v.findViewById(R.id.edNumero);

        return v;
    }

    public String[] retornaPalavraENumero() {
        String[] data = new String[2];
        data[0] = edPalavra.getText().toString();
        data[1] = edNumero.getText().toString();

        return data;
    }
}

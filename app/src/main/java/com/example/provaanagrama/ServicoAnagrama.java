package com.example.provaanagrama;

import android.app.IntentService;
import android.content.Intent;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ServicoAnagrama extends IntentService {
    public static String ACTION_RESPOSTA_SERVICO_ANAGRAMA = "resposta_servico_anagrama";

    public ServicoAnagrama() {
        super("ServicoAnagrama");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(intent != null) {
            String palavra = intent.getStringExtra("palavra");
            int numero = intent.getIntExtra("quantidade", 5);
            String[] anagramasCriados = new String[numero];

            int cont = 0;

            while(cont < numero) {
                anagramasCriados[cont] = criarAnagrama(palavra);
                cont++;

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            Intent resposta = new Intent(ACTION_RESPOSTA_SERVICO_ANAGRAMA);
            resposta.putExtra("anagramas", anagramasCriados);
            sendBroadcast(resposta);
        }
    }

    private String criarAnagrama(String palavra) {
        List<Character> listaPalavra = palavraToList(palavra);

        Collections.shuffle(listaPalavra);

        return listToPalavra(listaPalavra);
    }

    private List<Character> palavraToList(String palavra) {
        List<Character> lista = new LinkedList<>();

        for(int i = 0; i < palavra.length(); i++) {
            lista.add(palavra.charAt(i));
        }

        return lista;
    }

    private String listToPalavra(List<Character> lista) {
        StringBuilder sb = new StringBuilder();

        for(Character c : lista) {
            sb.append(c);
        }

        return sb.toString();
    }

}
package com.example.provaanagrama;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    FragmentAnagram fragmentAnagram;
    FragmentListAnagram fragmentListAnagram;

    List<String> anagramas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentAnagram = (FragmentAnagram) getFragmentManager().findFragmentByTag("fragAnagram");
        fragmentListAnagram = (FragmentListAnagram) getFragmentManager().findFragmentByTag("fragAnagramList");

        if(anagramas == null) {
            anagramas = new ArrayList<>();
        }

        AnagramasReceiver ar = new AnagramasReceiver();
        registerReceiver(ar, new IntentFilter(ServicoAnagrama.ACTION_RESPOSTA_SERVICO_ANAGRAMA));

        fragmentListAnagram.setAnagramas(anagramas);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    class AnagramasReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String[] anagramas = intent.getStringArrayExtra("anagramas");
            fragmentListAnagram.addAllToList(anagramas);
        }
    }

    public void iniciar(View v) {
        String[] palavraENumero = fragmentAnagram.retornaPalavraENumero();

        Intent intent = new Intent(getApplicationContext(), ServicoAnagrama.class);

        intent.putExtra("palavra", palavraENumero[0]);
        intent.putExtra("quantidade", Integer.parseInt(palavraENumero[1]));

        startService(intent);
    }

    public void removerTodas(MenuItem mi) {
        fragmentListAnagram.clearList();
    }

    public void removerSelecionadas(MenuItem mi) {
        fragmentListAnagram.clearSelected();
    }

    public void sair(MenuItem mi) {
        this.finish();
    }
}
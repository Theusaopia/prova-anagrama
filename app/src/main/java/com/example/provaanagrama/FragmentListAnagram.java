package com.example.provaanagrama;

import android.app.Fragment;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class FragmentListAnagram extends Fragment {
    ListView listViewAnagrama;
    List<String> anagramas = new ArrayList<>();
    ArrayAdapter adapter;

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_anagram_list, container, false);

        listViewAnagrama = (ListView) v.findViewById(R.id.listaAnagramas);

        return v;
    }

    public void setAnagramas(List<String> anagramas) {
        this.anagramas = anagramas;
        adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_multiple_choice, this.anagramas);
        listViewAnagrama.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listViewAnagrama.setAdapter(adapter);
    }

    public void addAllToList(String[] anagramasRecebidos) {
        for(int i = 0; i < anagramasRecebidos.length; i++) {
            this.anagramas.add(anagramasRecebidos[i]);
        }

        adapter.notifyDataSetChanged();
    }

    public void clearList() {
        anagramas.clear();
        adapter.notifyDataSetChanged();
    }

    public void clearSelected() {
        List<String> auxiliar = new ArrayList<>();
        auxiliar.addAll(this.anagramas);

        SparseBooleanArray checked = listViewAnagrama.getCheckedItemPositions();

        for(int i = 0; i < checked.size(); i++) {
            if(checked.valueAt(i)) { //verifica se o item esta marcado
                String anagrama = anagramas.get(checked.keyAt(i));
                int position = auxiliar.indexOf(anagrama);

                if(position != -1) {
                    listViewAnagrama.setItemChecked(checked.keyAt(i), false);
                    auxiliar.remove(position);
                }
            }
        }

        anagramas.clear();
        anagramas.addAll(auxiliar);
        adapter.notifyDataSetChanged();
    }

}
